﻿using BooksAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;

namespace BooksAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class BookController : Controller
    {
        private List<Book> _books = new List<Book>
        {
            //string connStr = "Data Source=(localdb)\\MSSQLLocalDB; Initial Catalog = BooksDb; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            //new Book
            //{
            //    BookId = 1,
            //    Title = "Misery",
            //    Author = "Steven King",
            //    Publisher = "McGraw",
            //    YearPublished = 1989,
            //    Price = 29.99
            //},
            //new Book
            //{
            //    BookId = 2,
            //    Title = "Adventures of Tom Sawyer",
            //    Author = "Mark Twain",
            //    Publisher = "Someone old",
            //    YearPublished = 1865,
            //    Price = 19.99
            //},
            //new Book
            //{
            //    BookId = 3,
            //    Title = "Get Rich Carefully",
            //    Author = "Jim Cramer",
            //    Publisher = "Money Bags",
            //    YearPublished = 2016,
            //    Price = 24.99
            //}
        };

        [HttpGet]
        public IEnumerable<Book> Get()
        {
            SqlConnection cn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog = BooksDb; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlCommand cmd = new SqlCommand("Select * from Book", cn);
            cn.Open();
            List<Book> bookList = new List<Book>();
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    Book book = new Book();
                    book.BookId = Convert.ToInt32(dr["BookId"]);
                    book.Title = (string)dr["Title"];
                    book.Author = (string)dr["Author"];
                    book.Publisher = (string)dr["Publisher"];
                    book.YearPublished = Convert.ToInt32(dr["YearPublished"]);
                    book.Price = Convert.ToDouble(dr["Price"]);
                    bookList.Add(book);
                }
            }
            cn.Close();
            return bookList;
        }

        [HttpGet("{id}")]
        public Book Get(int id)
        {
            SqlConnection cn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog = BooksDb; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlCommand cmd = new SqlCommand($"Select * from Book where BookId = {id}", cn);
            cn.Open();
            List<Book> bookList = new List<Book>();
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    Book book = new Book();
                    book.BookId = Convert.ToInt32(dr["BookId"]);
                    book.Title = (string)dr["Title"];
                    book.Author = (string)dr["Author"];
                    book.Publisher = (string)dr["Publisher"];
                    book.YearPublished = Convert.ToInt32(dr["YearPublished"]);
                    book.Price = Convert.ToDouble(dr["Price"]);
                    bookList.Add(book);
                }
            }
            cn.Close();
            return bookList[0];
            //return _books.FirstOrDefault(b => b.BookId == id);
        }

        [HttpPost]
        public ActionResult Post(Book inputBook)
        {
            //List<Book> bookList = new List<Book>();
            SqlConnection cn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog = BooksDb; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlCommand cmd = new SqlCommand("INSERT INTO Book(BookId, Title, Author, Publisher, YearPublished, Price) VALUES(" + inputBook.BookId + ",'" + inputBook.Title + "','" + inputBook.Author + "','" + inputBook.Publisher + "'," + inputBook.YearPublished + "," + inputBook.Price + ")", cn);
            cn.Open();

            int answer = cmd.ExecuteNonQuery();
            cn.Close();
            
            string location = $"api/books/book";
                return Ok();
               // return View(inputBook);
        }

        [HttpPut("{id}")]
        public ActionResult Put(Book iBook, int id)
        {
            SqlConnection cn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog = BooksDb; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            string cmdString = $"Update Book set BookId = {iBook.BookId}, Title = '{iBook.Title}', Author = '{iBook.Author}', Publisher = '{iBook.Publisher}', YearPublished = {iBook.YearPublished}, Price = {iBook.Price} where BookId = {id}";
            //string cmdString = $"Update Book set BookId = {iBook.BookId}, Title = {iBook.Title}, Author = {iBook.Author}, Publisher = {iBook.Publisher}, YearPublished = {iBook.YearPublished}, Price = {iBook.Price} where BookId = {id}";

            //inputBook.BookId + ",'" + inputBook.Title + "','" + inputBook.Author + "','" + inputBook.Publisher + "'," + inputBook.YearPublished + "," + inputBook.Price
            SqlCommand cmd = new SqlCommand(cmdString, cn);
            cn.Open();

            int answer = cmd.ExecuteNonQuery();
            cn.Close();
            return Ok();

        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {

            SqlConnection cn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog = BooksDb; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            string cmdString = $"Delete book where book.BookId = {id}";
            SqlCommand cmd = new SqlCommand(cmdString, cn);
            cn.Open();

            int answer = cmd.ExecuteNonQuery();
            cn.Close();
            //Book toBeDeleted = _books.FirstOrDefault(b => b.BookId == id);
            //if (toBeDeleted == null)
            //{
            //    return NotFound();
            //} else
            //{
            //    _books.Remove(toBeDeleted);
            //    return Ok();
            //}
            return Ok();
        }
    }
}
